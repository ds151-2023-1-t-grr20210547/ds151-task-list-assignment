import React, {useState} from 'react';
import { View, Text, FlatList, StyleSheet, TextInput, Button } from 'react-native';
import Item from '../components/Item';

function TodoScreen(){
    const [inputText, setInputText] = useState("");
    const [list, setList] = useState([]);
  
    const handleOk = (event) =>{
      if (event.nativeEvent.key === 'Enter') {
        setList([...list, {id:list.length , text:inputText}]);
        setInputText("");
        console.log(list);
      }    
  
    }
  
    return (
      <View>
        <Text style={{textAlign: "center"}}>TODO LIST</Text>
  
        <View>
          <TextInput 
            style={styles.input}
            value={inputText}
            onChangeText={setInputText}
            onKeyPress={handleOk}
          />
        </View>
  
        <View style={styles.container}>
          <FlatList
              data={list}
              renderItem={
                ({item}) => <Item text={item.text} />
              }
              keyExtractor={item => item.id}
          />
        </View>
        
      </View>
    );
}


const styles = StyleSheet.create(
    {
      view:{
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1,
      },
      container:{
        flex:1,
        padding: 20,
        flexGrow: 1,
      },
      input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
      },
      display:{
        flex:1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        height:20,
      }
    }
  );


export default TodoScreen;