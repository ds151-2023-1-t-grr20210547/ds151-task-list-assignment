import React, {useState} from 'react';
import { View, Text, Button } from 'react-native';

function HomeScreen({navigation}) {
  return (
    <View>
      <Text style={{textAlign: "center"}}>Home Screen</Text>
      <Button
          style={{width: 10}}
          title='Para ToDo List'
          onPress={()=> navigation.navigate('Todo')}
      />
    </View>
  );
}

export default HomeScreen;