import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const Item = ({ text }) => {
  const [feito, setFeito] = useState(false);

  const handlePress = () => {
    setFeito(!feito);
  };

  const textStyle = {
    textDecorationLine: feito ? "line-through" : "none",
  };
  const backStyle = {
    backgroundColor: feito ? "#ffc200" : "#888888",
  };
  return (
    <TouchableOpacity onPress={handlePress}>
      <View style={[styles.container, backStyle]}>
        <Text style={[styles.text, textStyle]}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#777",
    margin: 10,
  },
  text: {
    fontSize: 18,
    padding: 10,
  },
});

export default Item;
